# simple

https://docs.gitlab.com/ee/user/application_security/sast/

## 1- Initialisation

According the documentation: https://docs.gitlab.com/ee/user/application_security/sast/#configure-sast-in-your-cicd-yaml

```yaml
include:
  - template: Jobs/SAST.gitlab-ci.yml # will create a `semgrep-sast` job
```
without 
```yaml
stages:
  - test
```
Add a vulnerability: detected (on `main`)

## 2- Add a vulnerability with a MR

Add a vulnerability: detected (on `feature-branch`)

## 3 - Override the job and add a vulnerability with a MR
> don't show it with the tutorial
```yaml
# you can override the SAST Job
semgrep-sast:
  stage: test
  tags: [ saas-linux-large-amd64 ]
```
and add a new vulnerabilty

(on the same `feature-branch`) : detected

## Vulnerabilities "samples"

> `JaxRsEndpoint.java`
```java
// License: LGPL-3.0 License (c) find-sec-bugs
package endpoint;
import javax.ws.rs.Path;
import org.apache.commons.text.StringEscapeUtils;

@Path("/test")
public class JaxRsEndpoint {

    public String randomFunc(String s) {
        return s;
    }

    @Path("/hello0")
    public String hello0(String user) {
        return "Hello " + user; // BAD
    }

     @Path("/hello1")
    public String hello1(String user) {
        String tainted = randomFunc(user);
        return "Hello " + tainted;  // BAD
    }

    @Path("/hello2")
    public String hello2(String user) {
        String sanitized = StringEscapeUtils.unescapeJava(user);
        return "Hello " + sanitized; // OK
    }
} 

```

> `PseudoRandom.java`
```java
// License: LGPL-3.0 License (c) find-sec-bugs
package random;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.RandomUtils;

import java.util.Random;

public class PseudoRandom {
    static String randomVal =  Long.toHexString(new Random().nextLong());

    public static String generateSecretToken() {
        Random r = new Random();
        return Long.toHexString(r.nextLong());
    }

    public static String count() {
        return RandomStringUtils.random(10);
    }

    public static long getRandomVal() {
        return RandomUtils.nextLong();
    }
}

```

> `BadHexConversion.java`
```java
// License: LGPL-3.0 License (c) find-sec-bugs
package strings;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class BadHexConversion {

    public String danger(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] resultBytes = md.digest(text.getBytes("UTF-8"));

        StringBuilder stringBuilder = new StringBuilder();
        for(byte b :resultBytes) {
            stringBuilder.append( Integer.toHexString( b ) );
        }
        return stringBuilder.toString();
    }


    public String danger2(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] resultBytes = md.digest(text.getBytes("UTF-8"));

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0, resultBytesLength = resultBytes.length; i < resultBytesLength; i++) {
            byte b = resultBytes[i];
            stringBuilder.append(Integer.toHexString(b));
        }
        return stringBuilder.toString();
    }
}

```